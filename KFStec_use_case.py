import pandas as pd
import numpy as np
from sklearn import svm, datasets
import matplotlib.pyplot as plt
#matplotlib inline
import re
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from pathlib import Path
import os.path
from sklearn.metrics import confusion_matrix
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
import pylab
import scipy.stats as stats
from sklearn.metrics import mean_squared_error
from math import sqrt

######################importing the data##################################

def get_my_data(path, file, spreadsheet):
    """Get the data from a file"""
    my_file = Path(path+'\\'+file)
    try:
        os.path.isfile(path+'\\'+file)
        df = pd.ExcelFile(path + '\\' + file).parse(spreadsheet)
    except :
        print(path + '\\' + file + " file does not exist")

    return df


def get_dummies(df_1, df_2, columns):
    """create the dummy variables for categorical data in indicated columns in both provided dataframes"""
    temp_df = pd.DataFrame()
    # Iterating over all the common columns in train and test and score
    for col in columns:
        # Using complete data setsto form an exhaustive list of levels
        temp_data = [re.sub(str(col), '', str(i_str)) for i_str in pd.concat([df_1[col], df_2[col]])] #each value contains the column name - here we eliminate that entry
        dummy_mapping = pd.get_dummies(temp_data, prefix='col_' + col, dummy_na=True) #create dummy mapping
        temp_df = pd.concat([temp_df, dummy_mapping], axis=1) #create dummies

    cat_df_1 = temp_df.iloc[0:df_1.shape[0]]#divide into two datasets
    cat_df_2 = temp_df.iloc[df_1.shape[0]:]
    cat_df_2 = cat_df_2.set_index([list(range(cat_df_2.shape[0]))]) #reindex the dataframe

    return cat_df_1, cat_df_2

def normalise_data(df_1, df_2, columns):
    """normalise numerical data in indicated columns in both provided dataframes"""
    num_df_1 = pd.DataFrame()
    num_df_2 = pd.DataFrame()
    # Iterating over all the common columns in train and test and score
    for col in columns:
        # Using whole data to form an exhaustive list of levels
        data = pd.to_numeric(pd.concat([df_1[col], df_2[col]])) #concatenate the variables so to cover all values
        data = pd.DataFrame((data - min(data)) / (max(data) - min(data))) #normalise
        num_df_1 = pd.concat([num_df_1, data[0:df_1.shape[0]]], axis=1) #divide into separate dataframes
        num_df_2 = pd.concat([num_df_2, data[df_1.shape[0]:]], axis=1)
        num_df_2 = num_df_2.set_index([list(range(num_df_2.shape[0]))])  # reindex the dataframe
    return num_df_1, num_df_2

if __name__ == '__main__':
    print("\n-- import the data:")
    path = 'C:\\Users\\estekrz\\Desktop\\python_proj\\PMI_project'
    file = 'use_case_data.xlsx'
    spreadsheet = 'use_case_data'
    df1 = get_my_data(path, file, spreadsheet)

    path = 'C:\\Users\\estekrz\\Desktop\\python_proj\\PMI_project'
    file = 'score_data.xlsx'
    spreadsheet = 'Sheet1'
    df2 = get_my_data(path, file, spreadsheet)

#-----------------------------------------------------------------------------------------------------------------------
    print("\n-- preprocess the data:")
    print("\n---- create dummies for categorical data:")
#data preprocessing
#hardcoded categorical features
#an alternative:
    columns = ['REGION','MARKET','BRM',
               'BLDIMAGE','BRANDDIFFERENTIATOR','BRANDFAMILY','BRANDONMARKET',
               'BRANDSUBFAMILY','BRANDSUBFAMILYGROUP','BRANDSUBFAMILYGROUPING',
                'BRANDSUBFAMILYLINE','CHARINDICATOR','INDICATOR','RTYPE',
               'INTERNATIONALINDICATOR','ISREPLACEMENT','ISSTRATEGIC', 'ITEMSHAPE',
               'ITEMSCODE','LATESTPERIODINDEX', 'LENCATEGORY','PRICECLASS',
               'LOCALCLASS','MARKETEDBRAND','MINDICATOR','NPLLAUNCHDATE',
               'NPLLAUNCHYEAR','PCKT','PRICECLASSON','TCLASS','SPECIALFLAVOR','THICATEGORY',
               'TIPCOLOR', 'TRACKINGSTATUS']

#create dummies for categorical test (catdata_tt) and score (catdata_score) data
    catdata_tt, catdata_score = get_dummies(df1, df2, columns)
#alternative approach to extract 'object' variables and create dummies:
# obj_df = df.select_dtypes(include=['object']).copy()
# pd.get_dummies(obj_df, columns=columns, prefix=columns).head()
    print("\n---- normalise the numerical data:")
#hardcoded numerical values
    columns = ['BRMID','LEN', 'NCON','RETAILPACKPRICE','TRCONTE']
#normalise numerical test (numdata_tt) and score (numdata_score) data
    numdata_tt, numdata_score = normalise_data(df1, df2, columns)

#merge the numerical and the categorical data into a complete data sets for mdoel training/testing and latter scoring
    X_tt = pd.concat([catdata_tt, numdata_tt], axis=1)
    X_score = pd.concat([catdata_score, numdata_score], axis=1)

#-----------------------------------------------------------------------------------------------------------------------
    print("\n---- extract the response variable:")
    #get the market share data indicating the success rate
    response_variable = df1['Market_Share']
    cutoff=0.007
    response_variable = pd.DataFrame([0 if x < cutoff else 1 for x in response_variable])

    print("\n-- train the model:")

    X_train, X_test, y_train, y_test = train_test_split(X_tt, response_variable, test_size=0.2, random_state=0)

    #define the classifier
    #dt = RandomForestClassifier()
    dt = RandomForestClassifier(n_estimators=1000, min_samples_split=20, random_state=99)

    #train the classifier
    dt.fit(X_train, np.ravel(y_train))


    print("\n-- evaluate the model using the training dataset:")


    pred = dt.predict(X_train)
    fpr, tpr, thresholds = metrics.roc_curve(y_train, pred)
    #compute the classifier statistics when working on the trein dataset
    stat_train = pd.DataFrame()
    stat_train['auc'] = [metrics.auc(fpr, tpr)]
    stat_train['recall'] = [metrics.recall_score(y_train, pred)]  # comput the recall
    stat_train['precision'] = [metrics.precision_score(y_train, pred)]  # compute precision
    stat_train['accuracy'] = [metrics.accuracy_score(y_train, pred)]  # compute accuracy
    stat_train['average_precision'] = [metrics.average_precision_score(y_train, pred)]  # compute average precision
    stat_train['f1'] = [metrics.f1_score(y_train, pred)]  # Compute the F1 score, also known as balanced F-score or F-measure
    stat_train['tn'], stat_train['fp'], stat_train['fn'], stat_train['tp'] = confusion_matrix(np.ravel(y_train), np.ravel(pred)).ravel()
    stat_train['RMSE']=sqrt(mean_squared_error(y_train, pred))
    stat_train['error'] = [1 - stat_train['accuracy'].values]
    stat_train['conf_90_min'] = [stat_train['error'].values - 1.64 * sqrt((stat_train['error'] * (1 - stat_train['error'])) / pred.shape[0])]  # confidence interval is given by: model error +/- const * sqrt( (error * (1 - error)) / n) with const: 1.64 (90%), 1.96 (95%), 2.33 (98%), 2.58 (99%)
    stat_train['conf_90_max'] = [stat_train['error'].values + 1.64 * sqrt((stat_train['error'] * (1 - stat_train['error'])) / pred.shape[0])]
    print(stat_train)

    print("\n-- validate the model with the test dataset:")

    pred = dt.predict(X_test)
    fpr, tpr, thresholds = metrics.roc_curve(y_test, pred)
    # compute the classifier statistics when working on the test dataset
    stat_test = pd.DataFrame()
    stat_test['auc'] = [metrics.auc(fpr, tpr)]
    stat_test['recall'] = [metrics.recall_score(y_test, pred)]  # comput the recall
    stat_test['precision'] = [metrics.precision_score(y_test, pred)]  # compute precision
    stat_test['accuracy'] = [metrics.accuracy_score(y_test, pred)]  # compute accuracy
    stat_test['average_precision'] = [metrics.average_precision_score(y_test, pred)]  # compute average precision
    stat_test['f1'] = [metrics.f1_score(y_test, pred)]  # Compute the F1 score, also known as balanced F-score or F-measure
    stat_test['tn'], stat_test['fp'], stat_test['fn'], stat_test['tp'] = confusion_matrix(np.ravel(y_test), np.ravel(pred)).ravel()
    stat_test['RMSE']=sqrt(mean_squared_error(y_test, pred))
    stat_test['error'] = [1-stat_test['accuracy'].values]
    stat_test['conf_90_min'] = [stat_test['error'].values - 1.64 * sqrt( (stat_test['error'] * (1 - stat_test['error'])) / pred.shape[0])] #confidence interval is given by: model error +/- const * sqrt( (error * (1 - error)) / n) with const: 1.64 (90%), 1.96 (95%), 2.33 (98%), 2.58 (99%)
    stat_test['conf_90_max'] = [stat_test['error'].values + 1.64 * sqrt( (stat_test['error'] * (1 - stat_test['error'])) / pred.shape[0])]


    print(stat_test)

    #-----------------------------------------------------------------------------------------------------------------------

    print("\n-- score the new products:")
    prediction = dt.predict(X_score)
    print(prediction)
    prediction_proba = dt.predict_proba(X_score)
    print(prediction_proba)



